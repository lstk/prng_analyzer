﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace iBPO_1
{
    class Program
    {
        public static int[] IntToBinary(int numb)
        {
            String str = Convert.ToString(numb, 2);
            str = str.PadLeft(16, '0');                         //set the length of binary number
            var array = str.Select(ch => ch - '0').ToArray();
            return array;
        }
        
        public static int[] SumOfOnes(List<int> input)
        {
            int[] output = new int[16];
            int[] temp = new int[16];
            for (int i=0; i<input.Count; i++)
            {
                temp = IntToBinary(input[i]);
                for (int k = 0; k<16; k++)
                {
                    output[k] += temp[k];
                }
            }
            return output;
        }

        public static float ChiSqOnes(int[] input, int NumsInArray)
        {
            float output = 0F;
            for (int i = 0; i <= 15; i++)
            {
                output += (input[i] - ((float)(NumsInArray * 0.5F))) * (input[i] - ((float)(NumsInArray * 0.5F))) / ((float)(NumsInArray * 0.5F));
            }
            return output;
        }

        public static void rndNumResearch(List<int> input)
        {
            int[] numOfOnes = new int[6];
            List<int[]> num = new List<int[]>();
            for (int i = 0; i < input.Count; i++)
            {
                num.Add(input[i].ToString().Select(c => (int)char.GetNumericValue(c)).ToArray()); //так пишут ДЦП
            }

            for (int i = 0; i < num.Count; i++)
            {
                for (int k = 0; k < num[i].Length; k++)
                {
                    if (num[i][k] == 1)
                    {
                        numOfOnes[k]++;
                    }
                }
            }
            double powJ = Math.Pow(2, -5);
            double xi2 = ((numOfOnes[5] - input.Count * powJ) * (numOfOnes[5] - input.Count * powJ)) / (input.Count * powJ);

            for (int j = 1; j <= 5; j++)
            {
                powJ = ((5 - j + 1.0) / j) * powJ;
                xi2 += ((numOfOnes[j] - input.Count * powJ) * (numOfOnes[j] - input.Count * powJ)) / (input.Count * powJ);
            }
            Console.WriteLine(xi2 + " Chi squared of random digits research");
        }

        public static float ChiSquared(List<int> input)
        {
            int numberOfIntervals = 10;
            int[] groupes = new int[numberOfIntervals];
            int max = input.Max();
            int min = input.Min();
            double perf = Math.Ceiling(Convert.ToDouble(max - min) / numberOfIntervals);
            int border = Convert.ToInt32(perf);
            Console.WriteLine(border.ToString() + " Border");
            for (int i = 0; i<input.Count(); i++)
            {
                for (int k = 0; k < numberOfIntervals; k++)
                {
                    if (((min+(border*k)) <= input[i]) && (input[i] < (min + border*(k+1))))
                    {
                        groupes[k]++; break;
                    }
                }
            }

            int gall = 0;
            for (int i = 0; i < numberOfIntervals; i++)
            {
                gall = gall + groupes[i];
            }
            
            Console.WriteLine(gall.ToString() + " numbers in file read");

            float sum = 0;
            float Pj = 1F / numberOfIntervals;

            for (int i = 0; i < numberOfIntervals; i++ )
            {
                sum += ((groupes[i] - input.Count * Pj) * (groupes[i] - input.Count * Pj)) / (input.Count * Pj);
            }

            Console.WriteLine("Chi Squared = " + sum.ToString());
            Console.WriteLine("Number of freedom = " + (numberOfIntervals-1).ToString());            
            float output = 0;
            return sum;

        }

        public static void DrawMonoBitmap(string outputfilename, List<bool> arrayofbools)
        {
            int sizex = 0;
            int sizey = 0;
            sizex = System.Convert.ToInt32(Math.Floor(Math.Sqrt(System.Convert.ToDouble(arrayofbools.Count)))); //sqrt of (size of array)
            sizey = sizex;
            Color bl = Color.Black;
            Color wt = Color.White;
            using (var bmp = new Bitmap(sizex+1, sizey+1))
            using (var gr = Graphics.FromImage(bmp))
            {
                gr.FillRectangle(Brushes.Red, new Rectangle(0, 0, bmp.Width, bmp.Height)); //red frame
                int index = 0;
                for (int i = 1; i < sizex; i++)
                    for (int k = 1; k < sizey; k++)
                    {
                        if (arrayofbools[index] == false) bmp.SetPixel(i, k, bl);
                        else bmp.SetPixel(i, k, wt);
                        index++;
                    }
                var path = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), outputfilename);
                bmp.Save(path);
            }
            Console.WriteLine("Bitmap saved with name " + outputfilename + " on your desktop");
        }
        public static List<int> OpenFile(string path)
        {
            List<int> output= new List<int>();
            using (StreamReader r = new StreamReader(path))
            {
                string line;
                while ((line = r.ReadLine()) != null)
                {
                    output.Add(Convert.ToInt32(line));
                }
                Console.WriteLine("File Opened");
                return output;
            }
        }
        public static List<bool> ConvertToBoolArray(List<int> input)
        {
            var max = input.Max();
            var min = input.Min();
            var half = Math.Floor(System.Convert.ToDouble((max - min) / 2)) + min;
            List<bool> output = new List<bool>();
            for (int i = 0; i < input.Count; i++)
            {
                if (input[i] > half) output.Add(true);
                else output.Add(false);
            }
            Console.WriteLine("Converted to array of bools");
            return output;
        }

        static void Main(string[] args)
        {
            var values = OpenFile(@"C:\Users\Дмитрий\Desktop\50\Task1\0");
            var max = values.Max();
            var min = values.Min();

            Console.WriteLine("FILE  метод вычетов" + " OPENED");
            Console.WriteLine(max + " <- max | min -> " + min + " | continue");

            ChiSquared(values);

            rndNumResearch(values);

            float ChiSqBinaries = ChiSqOnes(SumOfOnes(values), values.Count);

            Console.WriteLine("\nChi Sq of Ones -> " + ChiSqBinaries.ToString("N3"));
            Console.ReadLine();
        }

    }
}
